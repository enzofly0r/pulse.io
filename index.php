<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500" rel="stylesheet">
  <link rel="stylesheet" href="css/color.min.css">
  <link rel="stylesheet" href="css/grid.min.css">
  <link rel="stylesheet" href="css/helpers.min.css">
  <link rel="stylesheet" href="css/style.min.css">
  <link rel="icon" href="webgallery/favico.png">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  <title>Pulse.io</title>
</head>
<body class="background">

  <!-- content gradient -->
  <section class="layer">
    <div class="modal-content">
      <!-- logo -->
      <div class="logo">
        <img src="webgallery/logo2.png" alt="" />
      </div>
      <!-- button group login & signup -->
      <div class="group-btn">
        <a href="#"><button id="login" class="btn purple display-modal" name="button">log in</button></a>
        <a href="#"><button id="signup" class="btn white display-modal" name="button">sign up</button></a>
      </div>
      <!-- modal connect & signup -->
      <article id="modal-connect" class="modal-connect">
      </article>
      <footer>contact us  -  my github   -   other work</footer>
    </div>
  </section>
  <!--  -->

</body>
</html>
