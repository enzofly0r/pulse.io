<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500" rel="stylesheet">
  <link rel="stylesheet" href="css/color.min.css">
  <link rel="stylesheet" href="css/grid.min.css">
  <link rel="stylesheet" href="css/helpers.min.css">
  <link rel="stylesheet" href="css/style.min.css">
  <link rel="icon" href="webgallery/favico.png">
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui.min.js"></script>
  <script type="text/javascript" src="js/script.js"></script>
  <title>Pulse.io</title>
</head>
<body>

  <header class="s12 m12 l12 xl12 background">
    <section class="layer align-center">
      <div class="container" id="header-profil">
        <article class="align-center">
          <div class="bubble-info">
            <a href="#" id="profil-user">
              <figure class="circle-info align-center">
                <i class="fa fa-user" aria-hidden="true"></i>
              </figure>
            </a>
          </div>
          <div class="bubble-info">
            <a href="#" id="message-user">
              <figure class="circle-info align-center">
                <i class="fa fa-commenting" aria-hidden="true"></i>
              </figure>
            </a>
          </div>
          <div class="bubble-info">
            <a href="#" id="music-user">
              <figure class="circle-info align-center">
                <i class="fa fa-music" aria-hidden="true"></i>
              </figure>
            </a>
          </div>
        </article>
        <article class="align-center">
          <figure class="my-avatar align-center" style="background-image: url(webgallery/avatar.jpg)">
            <p class="text-center">
              <img src="webgallery/logo2.png" alt="" />
            </p>
          </figure>
        </article>
      </div>
    </section>
  </header>
  <nav class="s12 m12 l12 xl12 white align-center" id="sub-menu">
    <ul class="align-center">
      <a href="#">
        <li class="sub-menu">
          <div class="s-hidden">
            friends
          </div>
          <div class="s-visible m-hidden l-hidden xl-hidden">
            <i class="fa fa-users" aria-hidden="true"></i>
          </div>
        </li>
      </a>
      <li>|</li>
      <a href="#">
        <li class="sub-menu">
          <div class="s-hidden">
            events
          </div>
          <div class="s-visible m-hidden l-hidden xl-hidden">
            <i class="fa fa-calendar" aria-hidden="true"></i>
          </div>
        </li>
      </a>
      <li>|</li>
      <a href="#">
        <li class="sub-menu">
          <div class="s-hidden">
            setting
          </div>
          <div class="s-visible m-hidden l-hidden xl-hidden">
            <i class="fa fa-cog" aria-hidden="true"></i>
          </div>
        </li>
      </a>
    </ul>
  </nav>
  <section class="container no-padding" id="content-profil-user">
    <article class="col s12 m8 l8 xl8">
      <div class="col s12 m12 l12 xl12 blue-lighten-2">
        this is my profil
      </div>
    </article>
    <article class="col s12 m4 l4 xl4">
      <div class="col s12 m12 l12 xl12 yellow-lighten-2">
        contact?
      </div>
    </article>
    <article class="col s6 m6 l3 xl3">
      <div class="col s12 m12 l12 xl12 purple-darken-3">
        one
      </div>
    </article>
    <article class="col s6 m6 l3 xl3">
      <div class="col s12 m12 l12 xl12 purple-lighten-3">
        two
      </div>
    </article>
    <article class="col s6 m6 l3 xl3">
      <div class="col s12 m12 l12 xl12 purple-darken-3">
        three
      </div>
    </article>
    <article class="col s6 m6 l3 xl3">
      <div class="col s12 m12 l12 xl12 purple-lighten-3">
        four
      </div>
    </article>
    <article class="col s12 m12 l12 xl12">
      <div class="col s12 m12 l12 xl12 turquoise-darken-3">
        oh my god.
      </div>
    </article>
  </section>
  <section class="container no-padding" id="content-message-user">
    <article class="col s12 m12 l12 xl6">
      <div class="col s12 m12 l12 xl12 red-lighten-1">
        hello world!
      </div>
    </article>
    <article class="col s8 m6 l8 xl8">
      <div class="col s12 m12 l12 xl12 blue-lighten-3">
        this is an input
      </div>
    </article>
    <article class="col s4 m6 l4 xl4">
      <div class="col s12 m12 l12 xl12 blue-darken-2">
        button
      </div>
    </article>
  </section>
  <section class="container no-padding" id="content-music-user">
    <article class="col s12 m4 l4 xl6">
      <div class="col s12 m12 l12 xl12 pink-lighten-1">
        my favorite songs
      </div>
    </article>
    <article class="col s6 m4 l4 xl6">
      <div class="col s12 m12 l12 xl12 grey-lighten-1">
        best songs ever
      </div>
    </article>
    <article class="col s6 m4 l4 xl6">
      <div class="col s12 m12 l12 xl12 turquoise-lighten-1">
        oyé
      </div>
    </article>
    <article class="col s12 m6 l6 xl6">
      <div class="col s12 m12 l12 xl12 turquoise-lighten-1">
        hmm..
      </div>
    </article>
    <article class="col s12 m6 l6 xl6">
      <div class="col s12 m12 l12 xl12 pink-lighten-1">
        music please.
      </div>
    </article>
  </section>
</body>
</html>
