$(document).ready(function(){
  /*
  * rotate logo dom load
  */
  $(".logo img").addClass("bounce-it");

  /*
  * display modal connect
  */
  $("#modal-connect").hide();


  $(".display-modal").click(function(){
    var loginModal = '<h5 class="title-modal">connexion</h5><div class="group-form"><input type="text" name="name" value="" placeholder="Username"><input type="password" name="name" value="" placeholder="Password"></div><a href="me.php"><button class="btn purple" name="button">connect</button></a>';
    var signModal = '<h5 class="title-modal">inscription</h5><div class="group-form"><input type="text" name="name" value="" placeholder="Username"><input type="password" name="name" value="" placeholder="Password"><input type="password" name="name" value="" placeholder="Repeat Password"></div><a href="me.php"><button class="btn purple" name="button">sign up</button></a>';
    var idBtn = $(this).attr("id");
    $(this).addClass("circle rotate-it").html("<i class=\"fa fa-refresh\" aria-hidden=\"true\"></i>");
    function displayConnect(){
      $(".group-btn").fadeOut(600);
      if(idBtn == "login"){
        $("#modal-connect").html(loginModal);
        function displayModal(){
          $("#modal-connect").fadeIn(500);
        }
        setTimeout(displayModal, 601);
      }
      else{
        $("#modal-connect").html(signModal);
        function displayModal(){
          $("#modal-connect").fadeIn(500);
        }
        setTimeout(displayModal, 601);
      }
    };
    setTimeout(displayConnect,2100);
  });

  // display logo on avatar profil
  $(".my-avatar").children('p').hide();
  $(".my-avatar").hover(function(){
    $(this).children('p').fadeToggle("slow");
  });
  //

  // hover effect on bubble-info
  $(".circle-info").hover(function(){
    $(this).toggleClass("rotate-it");
  });
  //

  // display categorie onclick bubble-info
  $("#content-friends-user").hide();
  $("#content-music-user").hide();
  //// display content profil
  $("#profil-user").click(function(){
    $("#content-friends-user").hide();
    $("#content-music-user").hide();
    $("#content-profil-user").fadeIn();
  });
  //// display content friends
  $("#friends-user").click(function(){
    $("#content-profil-user").hide();
    $("#content-music-user").hide();
    $("#content-friends-user").fadeIn();
  });
  //// display content music
  $("#music-user").click(function(){
    $("#content-profil-user").hide();
    $("#content-friends-user").hide();
    $("#content-music-user").fadeIn();
  });

});
